﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KGHelpDesk.Shared
{
	public class IssueDiagnoseModel
	{
		public int Id { get; set; }
		public DateTime DiagnosisDate { get; set; }
		public int[] Diagnoses { get; set; }
		public string Comment { get; set; }
	}
}
