﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KGHelpDesk.Shared
{
	public class Problem
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
