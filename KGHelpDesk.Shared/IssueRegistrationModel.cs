﻿using System;

namespace KGHelpDesk.Shared
{
	public class IssueRegistrationModel
	{
		public int Id { get; set; }
		public DateTime RegistrationDate { get; set; }
		public int[] Problems { get; set; }
		public string Comment { get; set; }
	}
}