﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KGHelpDesk.Shared
{
	public class CameraIssueExtendedResult
	{
		public string Camera { get; set; }
		public string RegistrationDate { get; set; }
		public string CurrentState { get; set; }
		public string ResolvementDate { get; set; }
		public string ActionStack { get; set; }
		public string ActionUserStack { get; set; }
		public string ActionCommentStack { get; set; }
	}
}
