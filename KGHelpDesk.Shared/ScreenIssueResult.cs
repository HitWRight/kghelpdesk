﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KGHelpDesk.Shared
{
    public class ScreenIssueResult
    {
        public int Id { get; set; }
        public string Screen { get; set; }
        public string Stage { get; set; }
        public string Comment { get; set; }

        public string ActionClassStack { get; set; }
		 public string User { get; set; }
    }
}
