﻿using System;

namespace KGHelpDesk.Shared
{
	public class IssueResolveReopenModel
	{
		public int Id { get; set; }
		public DateTime ResolveTime { get; set; }
		public string Comment { get; set; }
	}
}
