﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using KGHelpDesk.Shared;
using Microsoft.AspNetCore.Blazor;

namespace KGHelpDesk.Client
{
	public class AppState
	{
		public string LoggedOnUser { get; set; }

		private readonly HttpClient _client;

		public AppState(HttpClient client)
		{
			_client = client;
		}

		public async Task<bool> Login(AccountModel loginInfo)
		{
			
			var result = await _client.PostAsync("/api/Account/Login",
				new StringContent($"{{\"username\":\"{loginInfo.Username}\",\"password\":\"{loginInfo.Password}\"}}",
					Encoding.UTF8, "application/json"));
			bool isSuccess = result.IsSuccessStatusCode;
			                 
			if (isSuccess)
				LoggedOnUser = loginInfo.Username;

			return isSuccess;
		}

		public async Task<bool> Logout()
		{
			var result = await _client.PostAsync("api/Account/Logout", new StringContent(""));
			if(result.IsSuccessStatusCode)
			LoggedOnUser = "";

			return result.IsSuccessStatusCode;
		}
	}
}
