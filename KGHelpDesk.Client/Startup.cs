using System;
using System.Net.Http;
using Microsoft.AspNetCore.Blazor.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace KGHelpDesk.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
	        services.AddSingleton<AppState>();
	        //services.AddSingleton<HttpClient>(provider => new HttpClient() {BaseAddress = new Uri("http://localhost:51893/") });
        }

        public void Configure(IBlazorApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
