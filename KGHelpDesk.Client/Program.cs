﻿using Microsoft.AspNetCore.Blazor.Hosting;

namespace KGHelpDesk.Client
{
	public class Program
	{
		private static IWebAssemblyHost Host { get; set; }

		public static void Main(string[] args)
		{
			Host = CreateHostBuilder(args).Build();
			Host.Run();
		}

		public static void Restart()
		{
			Host.StopAsync().GetAwaiter().GetResult();
			Host.StartAsync().GetAwaiter().GetResult();
		}

		public static IWebAssemblyHostBuilder CreateHostBuilder(string[] args) =>
			BlazorWebAssemblyHost.CreateDefaultBuilder()
				.UseBlazorStartup<Startup>();
	}
}
