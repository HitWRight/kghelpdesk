﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KGHelpDesk.Server.Migrations
{
    public partial class TrackUserWhoMakesActions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Action",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Action");
        }
    }
}
