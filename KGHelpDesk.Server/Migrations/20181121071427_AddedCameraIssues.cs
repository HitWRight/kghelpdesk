﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KGHelpDesk.Server.Migrations
{
    public partial class AddedCameraIssues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
	        //migrationBuilder.DropTable(
		       // name: "ActionActionClassification");

	        //migrationBuilder.DropTable(
		       // name: "IssueAction");

	        //migrationBuilder.DropTable(
		       // name: "ActionClassification");

	        //migrationBuilder.DropTable(
		       // name: "Action");

	        //migrationBuilder.DropTable(
		       // name: "Issue");

	        //migrationBuilder.DropTable(
		       // name: "Cameras");

			migrationBuilder.CreateTable(
				 name: "Action",
				 columns: table => new
				 {
					 Id = table.Column<int>(nullable: false)
							.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					 Type = table.Column<int>(nullable: false),
					 Date = table.Column<DateTime>(nullable: false),
					 Comment = table.Column<string>(maxLength: 512, nullable: true)
				 },
				 constraints: table =>
				 {
					 table.PrimaryKey("PK_Action", x => x.Id);
				 });

			migrationBuilder.CreateTable(
				 name: "ActionClassification",
				 columns: table => new
				 {
					 Id = table.Column<int>(nullable: false)
							.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					 Name = table.Column<string>(maxLength: 255, nullable: false),
					 Discriminator = table.Column<string>(nullable: false)
				 },
				 constraints: table =>
				 {
					 table.PrimaryKey("PK_ActionClassification", x => x.Id);
				 });

			migrationBuilder.CreateTable(
                name: "Cameras",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cameras", x => x.Id);
                });

			migrationBuilder.CreateTable(
				 name: "ActionActionClassification",
				 columns: table => new
				 {
					 ActionId = table.Column<int>(nullable: false),
					 ActionClassificationId = table.Column<int>(nullable: false)
				 },
				 constraints: table =>
				 {
					 table.PrimaryKey("PK_ActionActionClassification", x => new { x.ActionId, x.ActionClassificationId });
					 table.ForeignKey(
							  name: "FK_ActionActionClassification_ActionClassification_ActionClassificationId",
							  column: x => x.ActionClassificationId,
							  principalTable: "ActionClassification",
							  principalColumn: "Id",
							  onDelete: ReferentialAction.Cascade);
					 table.ForeignKey(
							  name: "FK_ActionActionClassification_Action_ActionId",
							  column: x => x.ActionId,
							  principalTable: "Action",
							  principalColumn: "Id",
							  onDelete: ReferentialAction.Cascade);
				 });

			migrationBuilder.CreateTable(
				 name: "Issue",
				 columns: table => new
				 {
					 Id = table.Column<int>(nullable: false)
							.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					 Discriminator = table.Column<string>(nullable: false),
					 CameraId = table.Column<int>(nullable: true),
					 ScreenId = table.Column<int>(nullable: true)
				 },
				 constraints: table =>
				 {
					 table.PrimaryKey("PK_Issue", x => x.Id);
					 table.ForeignKey(
							  name: "FK_Issue_Cameras_CameraId",
							  column: x => x.CameraId,
							  principalTable: "Cameras",
							  principalColumn: "Id",
							  onDelete: ReferentialAction.Restrict);
				 });

			migrationBuilder.CreateTable(
				 name: "IssueAction",
				 columns: table => new
				 {
					 ActionId = table.Column<int>(nullable: false),
					 IssueId = table.Column<int>(nullable: false)
				 },
				 constraints: table =>
				 {
					 table.PrimaryKey("PK_IssueAction", x => new { x.ActionId, x.IssueId });
					 table.ForeignKey(
							  name: "FK_IssueAction_Action_ActionId",
							  column: x => x.ActionId,
							  principalTable: "Action",
							  principalColumn: "Id",
							  onDelete: ReferentialAction.Cascade);
					 table.ForeignKey(
							  name: "FK_IssueAction_Issue_IssueId",
							  column: x => x.IssueId,
							  principalTable: "Issue",
							  principalColumn: "Id",
							  onDelete: ReferentialAction.Cascade);
				 });

			migrationBuilder.CreateIndex(
				 name: "IX_ActionActionClassification_ActionClassificationId",
				 table: "ActionActionClassification",
				 column: "ActionClassificationId");

			migrationBuilder.CreateIndex(
				 name: "IX_Issue_CameraId",
				 table: "Issue",
				 column: "CameraId");

			migrationBuilder.CreateIndex(
				 name: "IX_IssueAction_IssueId",
				 table: "IssueAction",
				 column: "IssueId");
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActionActionClassification");

            migrationBuilder.DropTable(
                name: "IssueAction");

            migrationBuilder.DropTable(
                name: "ActionClassification");

            migrationBuilder.DropTable(
                name: "Action");

            migrationBuilder.DropTable(
                name: "Issue");

            migrationBuilder.DropTable(
                name: "Cameras");
        }
    }
}
