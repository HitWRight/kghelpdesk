using System.Collections.Generic;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Blazor.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    MediaTypeNames.Application.Octet,
                    WasmMediaTypeNames.Application.Wasm,
                });
            });

            var connectionString =
                "Server=10.0.110.249\\MSSQL2017STD;Database=KG_HelpDesk;User Id=HitWRight;Password=medialandas-puikus-PARTNERIS";
            services.AddDbContext<HelpDeskContext>(options => options.UseSqlServer(connectionString));

				services.Configure<CookiePolicyOptions>(options =>
	        {
		        options.CheckConsentNeeded = context => true;
		        options.MinimumSameSitePolicy = SameSiteMode.None;
	        });

	        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
		        .AddCookie();
	        services.AddHttpContextAccessor();
	        services.AddScoped<HttpContextAccessor>();
	        services.AddHttpClient();
	        services.AddScoped<HttpClient>();


		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

				//app.UseHttpsRedirection();
	        //app.UseStaticFiles();
	        app.UseCookiePolicy();
	        app.UseAuthentication();
	        app.UseMvc(routes =>

	        {

		        routes.MapRoute(name: "default",

			        template: "{controller=Home}/{action=Index}/{id?}");

	        });



			app.UseBlazor<Client.Startup>();
        }
    }
}
