﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KGHelpDesk.Server.EFModels
{
	public class Camera
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		[Required, StringLength(255), DisplayName("Pavadinimas")]
		public string Name { get; set; }

		public override string ToString() => Name;
	}
}
