﻿using System.ComponentModel.DataAnnotations;

namespace KGHelpDeskAPI.EFModel
{
	public class ScreenIssue : Issue
	{
		[Required] public int ScreenId { get; set; }
	}
}