﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KGHelpDeskAPI.EFModel
{
    public class CameraDiagnosis : ActionClassification
    {
        public override string ToString() => Name;
    }
}
