﻿using System.Collections.Generic;

namespace KGHelpDeskAPI.EFModel
{
    public class ScreenProblem : ActionClassification
    {
        public override string ToString() => Name;
    }
}
