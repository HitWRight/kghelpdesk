﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KGHelpDeskAPI.EFModel
{
    public class Action
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DisplayName("Veiksmo tipas")]
        public ActionTypes Type { get; set; }

        public ICollection<ActionActionClassification> Classifications { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [StringLength(512)]
        public string Comment { get; set; }

		 [Required]
		  public int UserId { get; set; }

        public ICollection<IssueAction> Issues { get; set; }
    }

    public enum ActionTypes {
        Register,
        Diagnose,
        Resolve,
        Reopen
    }
}
