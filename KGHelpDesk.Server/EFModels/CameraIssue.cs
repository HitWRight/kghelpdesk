﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KGHelpDesk.Server.EFModels;

namespace KGHelpDeskAPI.EFModel
{
    public class CameraIssue : Issue
    {
	    public Camera Camera { get; set; }
    }
}
