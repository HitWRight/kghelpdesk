﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KGHelpDesk.Server.EFModels;
using Microsoft.EntityFrameworkCore;

namespace KGHelpDeskAPI.EFModel
{
    public class HelpDeskContext : DbContext
    {
        public DbSet<ScreenProblem> ScreenProblems { get; set; }
        public DbSet<ScreenDiagnosis> ScreenDiagnoses { get; set; }
        public DbSet<ScreenIssue> ScreenIssues { get; set; }

		 public DbSet<CameraProblem> CameraProblems { get; set; }
		 public DbSet<CameraDiagnosis> CameraDiagnoses { get; set; }
		 public DbSet<CameraIssue> CameraIssues { get; set; }

		  public DbSet<Camera> Cameras { get; set; }

        public HelpDeskContext(DbContextOptions<HelpDeskContext> options) : base (options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //Issue <-mtm-> Action
            builder.Entity<IssueAction>()
                .HasKey(t => new { t.ActionId, t.IssueId });
            builder.Entity<IssueAction>()
                .HasOne(ia => ia.Action)
                .WithMany(a => a.Issues)
                .HasForeignKey(i => i.ActionId);
            builder.Entity<IssueAction>()
                .HasOne(ia => ia.Issue)
                .WithMany(i => i.Actions)
                .HasForeignKey(a => a.IssueId);

            //ActionClassification <-mtm-> Action
            builder.Entity<ActionActionClassification>()
                .HasKey(aac => new { aac.ActionId, aac.ActionClassificationId });
            builder.Entity<ActionActionClassification>()
                .HasOne(aac => aac.Action)
                .WithMany(a => a.Classifications)
                .HasForeignKey(c => c.ActionId);
            builder.Entity<ActionActionClassification>()
                .HasOne(aac => aac.ActionClassification)
                .WithMany(ac => ac.Actions)
                .HasForeignKey(a => a.ActionClassificationId);

            ////CameraProblem <-mtm-> CameraIssue
            //builder.Entity<CameraIssueProblem>()
            //    .HasKey(t => new { t.CameraIssueId, t.CameraProblemId });
            //builder.Entity<CameraIssueProblem>()
            //    .HasOne(cip => cip.CameraProblem)
            //    .WithMany(cp => cp.CameraIssues)
            //    .HasForeignKey(ci => ci.CameraProblemId)
            //    .OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<CameraIssueProblem>()
            //    .HasOne(cip => cip.CameraIssue)
            //    .WithMany(ci => ci.Problems)
            //    .HasForeignKey(p => p.CameraIssueId)
            //    .OnDelete(DeleteBehavior.Restrict);

            ////CameraProblem <-mtm-> CameraIssue
            //builder.Entity<CameraDiagnosisIssue>()
            //    .HasKey(t => new { t.CameraDiagnosisId, t.CameraIssueId });
            //builder.Entity<CameraDiagnosisIssue>()
            //    .HasOne(cdi => cdi.CameraDiagnosis)
            //    .WithMany(cd => cd.CameraIssues)
            //    .HasForeignKey(ci => ci.CameraDiagnosisId)
            //    .OnDelete(DeleteBehavior.Restrict);
            //builder.Entity<CameraDiagnosisIssue>()
            //    .HasOne(cdi => cdi.CameraIssue)
            //    .WithMany(ci => ci.Diagnoses)
            //    .HasForeignKey(d => d.CameraIssueId)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
        
    }
}
