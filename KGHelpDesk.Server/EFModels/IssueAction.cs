﻿namespace KGHelpDeskAPI.EFModel
{
    public class IssueAction
    {
        public Action Action { get; set; }
        public int ActionId { get; set; }
        public Issue Issue { get; set; }
        public int IssueId { get; set; }
    }
}
