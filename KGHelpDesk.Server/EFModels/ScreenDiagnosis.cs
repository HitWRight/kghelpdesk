﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KGHelpDeskAPI.EFModel
{
    public class ScreenDiagnosis : ActionClassification
    {
        public override string ToString() => Name;
    }
}
