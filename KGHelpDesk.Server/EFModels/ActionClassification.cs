﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KGHelpDeskAPI.EFModel
{
    public class ActionClassification 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, StringLength(255), DisplayName("Pavadinimas")]
        public string Name { get; set; }

        public ICollection<ActionActionClassification> Actions { get; set; }
    }
}
