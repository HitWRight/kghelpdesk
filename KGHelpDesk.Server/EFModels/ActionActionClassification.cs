﻿namespace KGHelpDeskAPI.EFModel
{
    public class ActionActionClassification
    {
        public Action Action { get; set; }
        public int ActionId { get; set; }
        public ActionClassification ActionClassification { get; set; }
        public int ActionClassificationId { get; set; }
    }
}