﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;

namespace KGHelpDeskAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	 [Authorize]
    public class ScreenProblemsController : Controller
    {
        private HelpDeskContext _context;

        public ScreenProblemsController(HelpDeskContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<ScreenProblem>> Get() => _context.ScreenProblems.ToList();

        [HttpGet("{id:int}")]
        public ActionResult<ScreenProblem> GetSingle(int id) =>
	        _context.ScreenProblems.SingleOrDefault(sp => sp.Id == id) ?? new ScreenProblem();

        [HttpPost]
        public IActionResult Post(ScreenProblem problem)
        {
	        var prob = _context.ScreenProblems.Find(problem.Id);
	        if (prob == null)
		        _context.ScreenProblems.Add(problem);
	        else
		        prob.Name = problem.Name;
	        _context.SaveChanges();
            return Ok();
        }
    }
}
