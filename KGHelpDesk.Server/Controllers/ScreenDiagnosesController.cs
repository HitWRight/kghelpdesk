﻿using System.Collections.Generic;
using System.Linq;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ScreenDiagnosesController : Controller
	{
		private readonly HelpDeskContext _context;

		public ScreenDiagnosesController(HelpDeskContext context) => _context = context;

		[HttpGet]
		public ActionResult<IEnumerable<ScreenDiagnosis>> Get() => _context.ScreenDiagnoses.ToList();

		[HttpGet("{id:int}")]
		public ActionResult<ScreenDiagnosis> GetSingle(int id) =>
			_context.ScreenDiagnoses.SingleOrDefault(cd => cd.Id == id) ?? new ScreenDiagnosis();

		[HttpPost]
		public IActionResult Post(ScreenDiagnosis diagnosis)
		{
			var diag = _context.ScreenDiagnoses.Find(diagnosis.Id);
			if (diag == null)

				_context.ScreenDiagnoses.Add(diagnosis);
			else
				diag.Name = diagnosis.Name;
			_context.SaveChanges();
			return Ok();
		}
	}
}