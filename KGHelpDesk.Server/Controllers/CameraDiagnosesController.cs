﻿using System.Collections.Generic;
using System.Linq;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class CameraDiagnosesController : Controller
	{
		private readonly HelpDeskContext _context;

		public CameraDiagnosesController(HelpDeskContext context) => _context = context;

		[HttpGet]
		public ActionResult<IEnumerable<CameraDiagnosis>> Get() => _context.CameraDiagnoses.ToList();

		[HttpGet("{id:int}")]
		public ActionResult<CameraDiagnosis> GetSingle(int id) => _context.CameraDiagnoses.SingleOrDefault(c => c.Id == id) ?? new CameraDiagnosis();

		[HttpPost]
		public IActionResult Post(CameraDiagnosis diagnosis)
		{
			var camDiag = _context.CameraDiagnoses.Find(diagnosis.Id);
			if (camDiag == null)
				_context.CameraDiagnoses.Add(diagnosis);
			else
				camDiag.Name = diagnosis.Name;
			_context.SaveChanges();
			return Ok();
		}
	}
}