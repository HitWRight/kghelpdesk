﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KGHelpDesk.Server.EFModels;
using KGHelpDesk.Shared;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
	[AllowAnonymous]
	[Route("api/[controller]")]
	public class AccountController : Controller
	{
		[HttpGet]
		public ActionResult<string> Get()
		{
			return Ok(HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Name)?.Value ?? "");
		}

		[HttpPost("Login")]
		public async Task<IActionResult> Login([FromBody]AccountModel account)
		{
			var loginId = CheckLogin(account.Username, account.Password);
			if (loginId != -1)
			{
				await HttpContext.SignInAsync(
					new ClaimsPrincipal(new ClaimsIdentity(new[]
					{
						new Claim(ClaimTypes.Name, account.Username),
						new Claim(ClaimTypes.Sid, loginId.ToString()), 
					}, CookieAuthenticationDefaults.AuthenticationScheme)));
				return Ok();
			}

			return BadRequest();
		}

		[HttpPost("Logout")]
		public async Task<IActionResult> Logout()
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			return Ok();
		}
	

		private int CheckLogin(string username, string password)
		{
			if(password == "AcmEkranai123")
				switch (username)
				{
					case "EdasC":
						return 22;
					case "VytautasB":
						return 23;
				}

			return -1;
		}

		public static string GetUser(int userId)
		{
			switch (userId)
			{
				case 22:
					return "Edas";
				case 23:
					return "Vytautas";
			}
			return "";
		}
	}
}
