﻿using System.Collections.Generic;
using System.Linq;
using KGHelpDesk.Server.EFModels;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
    [Route("api/[controller]")]
	 [ApiController]
	 [Authorize]
    public class CamerasController : Controller
    {
	    private readonly HelpDeskContext _context;

	    public CamerasController(HelpDeskContext context) => _context = context;

	    internal IEnumerable<Camera> GetCameras() => _context.Cameras;

	    [HttpGet]
	    public ActionResult<IEnumerable<Camera>> Get() => GetCameras().ToList();

	    [HttpGet("{id:int}")]
	    public ActionResult<Camera> GetCamera(int id) => _context.Cameras.SingleOrDefault(c => c.Id == id) ?? new Camera();

	    [HttpPost]
	    public IActionResult Post(Camera camera)
	    {
			 var cam = _context.Cameras.SingleOrDefault(c => c.Id == camera.Id);
			if (cam == null)
				_context.Cameras.Add(camera);
			else
			{
				_context.Cameras.Find(camera.Id).Name = camera.Name;
			}
			 _context.SaveChanges();
		    return Ok();
	    }
    }
}
