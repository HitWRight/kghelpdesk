﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Common.Extensions;
using KGHelpDesk.Server.Controllers;
using KGHelpDesk.Shared;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Action = KGHelpDeskAPI.EFModel.Action;

namespace KGHelpDeskAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class CameraIssuesController : Controller
	{
		private readonly HelpDeskContext _context;

		public CameraIssuesController(HelpDeskContext context) => _context = context;


		[HttpGet]
		public ActionResult<IEnumerable<CameraIssueResult>> Get()
		{
			var cameras = _context.Cameras.ToList();

			var l = _context.CameraIssues.Include(si => si.Camera).Include(si => si.Actions).Include("Actions.Action")
				.Include("Actions.Action.Classifications")
				.Include("Actions.Action.Classifications.ActionClassification").ToList().Select(si =>
					new CameraIssueResult
					{
						Comment = si.Actions.LastOrDefault().Action.Comment ?? "",
						Id = si.Id,
						Camera = cameras.SingleOrDefault(s => s.Id == si.Camera.Id)?.Name ?? "",
						Stage = si.Actions.LastOrDefault().Action.Type.ToString() ?? "",
						ActionClassStack = string.Join(";;",
							si.Actions.Select(a =>
								string.Join(";",
									a.Action.Classifications.Select(c => c.ActionClassification.Name)))),
						User = AccountController.GetUser(si.Actions.Last().Action.UserId)
					}).ToList();
			return l;
		}

		[HttpGet("Extended")]
		public ActionResult<IEnumerable<CameraIssueExtendedResult>> GetExtended() => _context.CameraIssues.Include(si => si.Camera).Include(si => si.Actions).Include("Actions.Action")
			.Include("Actions.Action.Classifications")
			.Include("Actions.Action.Classifications.ActionClassification").ToList().Select(si =>
				new CameraIssueExtendedResult
				{

					RegistrationDate = si.Actions.FirstOrDefault(a => a.Action.Type == ActionTypes.Register)?.Action.Date.ToString("yyyy-MM-dd HH:mms:ss") ?? "",
					ActionStack = string.Join(";;",
						si.Actions.Select(a =>
							string.Join(";",
								a.Action.Classifications.Select(c => c.ActionClassification.Name)))),
					ActionCommentStack = string.Join(";;",
						si.Actions.Select(a => a.Action.Comment)),
					ActionUserStack = string.Join(";;", si.Actions.Select(a => AccountController.GetUser(a.Action.UserId))),
					CurrentState = si.Actions.Last().Action.Type.ToString(),
					ResolvementDate = si.Actions.FirstOrDefault(a => a.Action.Type == ActionTypes.Resolve)?.Action.Date.ToString("yyyy-MM-dd HH:mm:ss") ?? "",
					Camera = si.Camera.Name
				}
			).ToList();

		[HttpGet("{id:int}")]
		public ActionResult<CameraIssueResult> Get(int id)
		{
			var cameras = _context.Cameras.ToList();

			var cameraIssue =  _context.CameraIssues.Include(si => si.Camera).Include(si => si.Actions).Include("Actions.Action")
				.Include("Actions.Action.Classifications")
				.Include("Actions.Action.Classifications.ActionClassification").Single(si => si.Id == id);
			return new CameraIssueResult
			{
				Comment = cameraIssue.Actions.LastOrDefault().Action.Comment ?? "",
				Id = cameraIssue.Id,
				Camera = cameras.SingleOrDefault(s => s.Id == cameraIssue.Camera.Id)?.Name ?? "",
				Stage = cameraIssue.Actions.LastOrDefault().Action.Type.ToString() ?? "",
				ActionClassStack = string.Join(";;",
					cameraIssue.Actions.Select(a =>
						string.Join(";",
							a.Action.Classifications.Select(c => c.ActionClassification.Name)))),
				User = AccountController.GetUser(cameraIssue.Actions.Last().Action.UserId)
			};
		}

		[HttpPost("Register")]
		public IActionResult RegisterIssue(IssueRegistrationModel regitrationModel)
		{
			var classifications =
				_context.CameraProblems.Where(sp => regitrationModel.Problems.Contains(sp.Id)).ToList();

			//Create Action
			var act = new Action
			{
				Comment = regitrationModel.Comment, Date = regitrationModel.RegistrationDate, Type = ActionTypes.Register,
				UserId = Convert.ToInt32(HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.Sid).Value)

			};
			act.Classifications = classifications
				.Select(sp => new ActionActionClassification {Action = act, ActionClassification = sp}).ToList();
			_context.Add(act);


			_context.SaveChanges();
			////Create Issue
			var issue = new CameraIssue {Camera = _context.Cameras.Single(c => c.Id == regitrationModel.Id)};
			issue.Actions = new List<IssueAction>();
			issue.Actions.Add(new IssueAction {Action = act});
			_context.Add(issue);

			_context.SaveChanges();

			using (var wr = new StreamWriter(GetIssueFile(issue)))
			{
				wr.WriteLine("");
			}
			return Ok();
		}

		[HttpPut("Diagnose")]
		public IActionResult DiagnoseIssue(IssueDiagnoseModel model)
		{
			var issue = _context.CameraIssues.Include(si => si.Camera).Include(si => si.Actions).Include("Actions.Action")
				.Include("Actions.Action.Classifications")
				.Include("Actions.Action.Classifications.ActionClassification").Single(si => si.Id == model.Id);

			switch (issue.Actions.Last().Action.Type)
			{
				case ActionTypes.Register:
				case ActionTypes.Reopen:
					break;
				default:
					return Forbid();
			}

			issue.Actions.Add(new IssueAction
			{
				Action = new Action
				{
					Comment = model.Comment, Date = model.DiagnosisDate,
					Type = ActionTypes.Diagnose,
					UserId = Convert.ToInt32(HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.Sid).Value)
				}.Do(a => a.Classifications = _context.CameraDiagnoses.Where(cd => model.Diagnoses.Contains(cd.Id))
					.ToList().Select(sp => new ActionActionClassification {Action = a, ActionClassification = sp}).ToList())
			});
			_context.SaveChanges();
			using (var wr = new StreamWriter(GetIssueFile(issue), true))
			{
				wr.WriteLine("Diagnosed:" + model.Diagnoses[0]);
			}
			return Ok();
		}

		[HttpPut("Resolve")]
		public IActionResult ResolveIssue(IssueResolveReopenModel model)
		{
			var issue = _context.CameraIssues.Include(si => si.Camera).Include(si => si.Actions).Include("Actions.Action")
				.Include("Actions.Action.Classifications")
				.Include("Actions.Action.Classifications.ActionClassification").Single(si => si.Id == model.Id);

			switch (issue.Actions.Last().Action.Type)
			{
				case ActionTypes.Diagnose:
					break;
				default:
					return Forbid();
			}

			issue.Actions.Add(new IssueAction
			{
				Action = new Action
				{
					Comment = model.Comment,
					Date = model.ResolveTime,
					Type = ActionTypes.Resolve,
					UserId = Convert.ToInt32(HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.Sid).Value)
				}
			});
			_context.SaveChanges();
			System.IO.File.Delete(GetIssueFile(issue));
			return Ok();
		}

		private string GetIssueFile(CameraIssue issue) => Path.Combine("C:\\Zabbix\\CameraIssues",
			$"{issue.Camera.Id}.{issue.Actions.First().Action.Date.ToString("yyyyMMdd.HHmmssffff")}.txt");
	}
}