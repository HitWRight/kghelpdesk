﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using KGHelpDesk.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
	[Route("api/[controller]")]
	[Authorize]
	public class ScreensController : Controller
	{
		internal static IEnumerable<Screen> GetScreens()
		{
			var result = new List<Screen>();

			var conn = new SqlConnection(
				"server=10.0.110.249;initial catalog=AURORADB;user id=HitWRight;password=medialandas-puikus-PARTNERIS");

			var comm = new SqlCommand("select Id, Name from scr.Screens", conn);
			conn.Open();
			var reader = comm.ExecuteReader();
			while (reader.HasRows && reader.Read())
				result.Add(new Screen {Id = reader.GetInt32(0), Name = reader.GetString(1)});

			return result;
		}

		[HttpGet]
		public ActionResult<IEnumerable<Screen>> Get() => GetScreens().ToList();
	}
}