﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using KGHelpDeskAPI.EFModel;
using Microsoft.AspNetCore.Authorization;

namespace KGHelpDeskAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	 [Authorize]
    public class CameraProblemsController : Controller
    {
        private HelpDeskContext _context;

        public CameraProblemsController(HelpDeskContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<CameraProblem>> Get() => _context.CameraProblems.ToList();

        [HttpGet("{id:int}")]
        public ActionResult<CameraProblem> GetSingle(int id) => _context.CameraProblems.SingleOrDefault(cp => cp.Id == id) ?? new CameraProblem();

        [HttpPost]
        public IActionResult Post(CameraProblem problem)
        {
			var camDiag = _context.CameraProblems.Find(problem.Id);
			if (camDiag == null)
				_context.CameraProblems.Add(problem);
			else
				camDiag.Name = problem.Name;
			_context.SaveChanges();
			return Ok();
		}
    }
}
