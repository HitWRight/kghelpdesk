﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Extensions
{
    public static class ActionExtensions
    {
        public static T Do<T>(this T obj, Action<T> action)
        {
            action(obj);
            return obj;
        }
    }
}
